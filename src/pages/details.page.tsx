import {gql, useQuery} from "@apollo/client";
import { useParams } from "react-router-dom";
import {Badge} from "antd";
import React from "react";

const SINGLE_QUERY = gql(`
query getInvoice($id: ID!){
  invoice(id:$id){
    id
    fullName
    typeDocument
    numberDocument
    dateRequest
    address
    codeRequest
    status
    
  }
}
`)

const DetailsPage = () => {
    const { id } = useParams();

    const { loading, error, data} = useQuery(SINGLE_QUERY, {variables: {id: id}})

    if(loading) return <p>Loading ...</p>
    if(error) return <p>{error.message}</p>

    const {codeRequest, dateRequest, fullName, numberDocument, status, typeDocument, address} = data.invoice;


    console.log(status)
    return <>
        <h2 className="text-sm text-custom_blue">Detalles</h2>
        <hr className="border-2 border-dashed border-custom my-4"/>
        <p className="text-md"><strong>Nombre completo:</strong> {fullName}</p>
        <p className="text-md"><strong>Codigo pedido:</strong> {codeRequest}</p>
        <p className="text-md"><strong>Número pedido:</strong> {numberDocument}</p>
        <p className="text-md"><strong>Tipo Documento:</strong> {typeDocument}</p>
        <p className="text-md"><strong>Fecha pedido:</strong> {dateRequest}</p>
        <p className="text-md"><strong>Dirección:</strong> {address} </p>
        <p className="text-md"><strong>Estado:</strong> {!status ? <Badge status="error" text="Inactivo"/> :
            <Badge status="success" text="activo"/>}</p>
    </>
}

export default DetailsPage;
