import {Pagination, PaginationProps, Table} from "antd";
import React, {useEffect, useState} from "react";
import {PlusOutlined} from '@ant-design/icons';

import {Link} from "react-router-dom";

import {DataType} from "../interfaces/invoices.interface";
import SearchComponent from "../components/search.component";

import {gql, useLazyQuery, useQuery} from '@apollo/client';
import {columns} from "../helpers/columns_table";

const QUERY = gql(`
    query getInvoices($offset: Int, $limit: Int){
        invoices(
            offset: $offset
            limit: $limit
        ){
            id
            codeRequest
            fullName
            dateRequest
            status
        }
    }
`);

const QUERY_COUNT = gql(`
    query countInvoices {
        countInvoices
    }`);



const InvoicePage = () => {
    const [getPage , setPage] = useState(1);

    const [getInvoice, {loading, data, error}] = useLazyQuery(QUERY);
    const {loading: loadingCount, data: dataCount, error: errorCount } = useQuery(QUERY_COUNT);


    useEffect(() => {
        getInvoice({
            variables: {
                offset: 1,
                limit: 10
            }
        })
    },[getInvoice])

    const onChange: PaginationProps['onChange'] = (pageNumber) => {
        setPage(pageNumber);
        getInvoice({
            variables: {
                offset: pageNumber,
                limit: 10
            }
        });
    };


    if (loading || loadingCount) return <p>Loader...</p>
    if (error || errorCount) return <p>{errorCount?.message} {error?.message}</p>

    const tableData: DataType[] = [];

    if (data) {
        data.invoices.map((items: DataType, index: number) => {
            tableData.push({
                key: index,
                id: items.id,
                codeRequest: items.codeRequest,
                fullName: items.fullName,
                dateRequest: items.dateRequest,
                status: items.status
            });
            return null;
        })
    }


    return <>
        <div className="text-center mb-6 bg-custom_blue p-4 rounded">
            <h3 className="text-white mb-4 font-bold text-sm">Encuentra Tú Pedido</h3>
            <SearchComponent/>
        </div>
        <header className="flex gap-2">
            <div className="w-10/12"></div>
            <div className="w-2/12">
                <Link to="/form"
                      className="flex w-12/12 justify-center items-center inline-block px-6 py-4 bg-custom text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:text-white hover:bg-custom hover:shadow-lg focus:bg-custom focus:shadow-lg focus:outline-none focus:ring-0 active:bg-custom active:shadow-lg transition duration-150 ease-in-out">
                    <PlusOutlined className="mr-1.5"/>Crear Pedido</Link>
            </div>
        </header>

        <Table columns={columns} dataSource={tableData} pagination={false}/>
        <div className="flex flex-row-reverse">
        <Pagination className="my-4" defaultCurrent={getPage} total={dataCount.countInvoices} onChange={onChange}/>
        </div>
    </>
}
export default InvoicePage;
