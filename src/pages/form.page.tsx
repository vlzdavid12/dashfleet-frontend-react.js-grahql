import {useState} from "react";
import {Formik, Form, Field} from "formik";
import {Alert, DatePicker, Switch} from 'antd';
import {useMutation, gql} from "@apollo/client";
import * as Yup from 'yup';
import moment from 'moment'
import dayjs from 'dayjs';

import {
    SaveOutlined,
    DownOutlined
} from '@ant-design/icons';

import {Values} from "../interfaces/form.interface";

const NEW_INVOICE = gql`
    mutation createInvoice($fullName: String!, $codeRequest: String!, $typeDocument: String!, $numberDocument: Int!, $address: String!, $dateRequest: String!, $status: Boolean!) {
        createInvoice(createInvoiceInput:{
            fullName: $fullName,
            codeRequest: $codeRequest,
            dateRequest: $dateRequest,
            numberDocument: $numberDocument,
            address: $address
            status: $status,
            typeDocument: $typeDocument,
        }){
            id
        }
    }
`

const validationSchema = Yup.object().shape({
    fullName: Yup.string().required('El nombre completo es requerido.'),
    codeRequest: Yup.string().required('Este campo codigo del pedido es requerido.'),
    typeDocument: Yup.string().required('Este campo tipo de documento es requerido.'),
    address:  Yup.string().required('Este campo dirección es requerido.'),
    numberDocument: Yup.number().required('Este campo número del documento es requerido.').positive('No se acepta números negativos').integer('El número debe ser entero'),
    dateRequest: Yup.date().required('La fecha del pedido es requerido.')
});

const FormPage = () => {
    const [isActive, setActive] = useState(true);
    const [isSuccess, setSuccess] = useState(false);
    const [createInvoice] = useMutation(NEW_INVOICE);

    const dateFormat = 'YYYY-MM-DD';

    let initial: Values = {
        fullName: '',
        codeRequest: '',
        typeDocument: '',
        numberDocument: '',
        address: '',
        dateRequest: moment().format(dateFormat),
        status: isActive,
    }

    const onChange = (checked: boolean) => {
       setActive(checked);
    };

    return <>
        { isSuccess ? (<Alert
            message="¡Insertado Correctamente!"
            description="Se han ingresado nuevos datos."
            type="success"
            showIcon
        />) :  ''}
    <Formik
        initialValues={initial}
        validationSchema={validationSchema}
        onSubmit={async (
            values: Values,
            formikHelpers
        ) => {
                values.status = isActive;

                try{
                   await createInvoice({
                       variables:{
                           fullName: values.fullName,
                           codeRequest: values.codeRequest,
                           typeDocument: values.typeDocument,
                           numberDocument: values.numberDocument,
                           dateRequest: values.dateRequest,
                           address: values.address,
                           status: values.status
                       }
                   });

                   formikHelpers.resetForm();
                   setSuccess(true);

                }catch (error){
                    console.log(error)
                }
        }}>

        {({errors, touched}) => (
            <Form>
                <section className="relative my-4">
                    <label>Nombre completo del representante.</label>
                    <Field className="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        my-2
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      " name="fullName"
                           placeholder="Ingresa el nombre completo"
                    />
                    {errors.fullName && touched.fullName ? (
                        <div className="mt-2 bg-red-100 rounded-lg py-2 px-4 mb-2 text-base text-red-700 mb-2"
                             role="alert">
                            {errors.fullName}
                        </div>
                    ) : null}
                </section>
                <section className="relative my-4">
                    <label>Número pedido.</label>
                    <Field className="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        my-2
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      " name="codeRequest"
                           placeholder="Ingresa número del pedido"
                    />
                    {errors.codeRequest && touched.codeRequest ? (
                        <div className="mt-2 bg-red-100 rounded-lg py-2 px-4 mb-2 text-base text-red-700 mb-2"
                             role="alert">
                            {errors.codeRequest}
                        </div>
                    ) : null}
                </section>
                <section className="relative my-4">
                    <label>Tipo del pedido.</label>
                    <Field as="select" className="
                form-select appearance-none
      block
      w-full
      px-3
      py-1.5
      text-base
      font-normal
      text-gray-700
      bg-white bg-clip-padding bg-no-repeat
      border border-solid border-gray-300
      rounded
      transition
      ease-in-out
      my-2
      focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                           placeholder="Seleccione tipo de pedido"
                           name="typeDocument">
                        <option value="">Ingresa el tipo de pedido</option>
                        <option value="a">Por objetivo</option>
                        <option value="b">Por forma</option>
                        <option value="c">Por el proveedor</option>
                        <option value="d">Por destino</option>
                    </Field>
                    <DownOutlined className="absolute top-10 right-3"/>
                    {errors.typeDocument && touched.typeDocument ? (
                        <div className="mt-2 bg-red-100 rounded-lg py-2 px-4 mb-2 text-base text-red-700 mb-2"
                             role="alert">
                            {errors.typeDocument}
                        </div>
                    ) : null}
                </section>
                <section className="relative my-4">
                    <label>Número Documento.</label>
                    <Field className="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        my-2
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
                           maxLength="10"
                           type="number"
                           name="numberDocument"
                           placeholder="Ingresa número del documento"
                    />
                    {errors.numberDocument && touched.numberDocument ? (
                        <div className="mt-2 bg-red-100 rounded-lg py-2 px-4 mb-2 text-base text-red-700 mb-2"
                             role="alert">
                            {errors.numberDocument}
                        </div>
                    ) : null}
                </section>
                <section className="relative my-4">
                    <label>Fecha Pedido.</label>
                    <DatePicker style={{ width: '100%'}} className="my-2"  defaultValue={dayjs(moment().format(dateFormat), dateFormat)}   name="dateRequest"/>
                    {errors.dateRequest && touched.dateRequest ? (
                        <div className="mt-2 bg-red-100 rounded-lg py-2 px-4 mb-2 text-base text-red-700 mb-2"
                             role="alert">
                            {errors.dateRequest}
                        </div>
                    ) : null}
                </section>
                <section className="relative my-4">
                    <label>Dirección.</label>
                    <Field className="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        my-2
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      " name="address"
                           placeholder="Ingresa la dirección"
                    />
                    {errors.address && touched.address ? (
                        <div className="mt-2 bg-red-100 rounded-lg py-2 px-4 mb-2 text-base text-red-700 mb-2"
                             role="alert">
                            {errors.address}
                        </div>
                    ) : null}
                </section>
                <section className="my-4 grid">
                    <label className="my-1">Estado.</label>
                    <Switch size="default" checkedChildren="Activo" unCheckedChildren="Inactivo" onChange={onChange} defaultChecked  />
                </section>
                <button
                    className="flex items-center justify-center gap-2 inline-block px-6 py-2.5 bg-custom text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-custom hover:text-white hover:shadow-lg focus:bg-custom focus:shadow-lg focus:outline-none focus:ring-0 active:bg-custom active:shadow-lg transition duration-150 ease-in-out"
                    type="submit">
                    <SaveOutlined /> Guardar
                </button>
            </Form>
        )}

    </Formik>
    </>
}
export default FormPage;
