import {ApolloClient, InMemoryCache} from "@apollo/client";

const client = new ApolloClient({
    connectToDevTools: true,
    uri: process.env.REACT_APP_URL_BACK,
    cache: new InMemoryCache()
})

export default  client;
