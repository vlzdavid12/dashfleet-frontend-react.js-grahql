import {ColumnsType} from "antd/es/table";
import {DataType} from "../interfaces/invoices.interface";
import {Badge} from "antd";
import {Link} from "react-router-dom";
import {FileTextOutlined} from "@ant-design/icons";
import React from "react";

export const columns: ColumnsType<DataType> = [
    {
        title: 'Número Pedido',
        dataIndex: 'codeRequest',
        key: 'codeRequest'
    },
    {
        title: 'Nombre Completo',
        dataIndex: 'fullName',
        key: 'fullName',
    },
    {
        title: 'Fecha Pedido',
        dataIndex: 'dateRequest',
        key: 'dateRequest',
    },
    {
        title: 'Estado',
        dataIndex: 'status',
        key: 'status',
        render: (_, {status}: DataType) => (!status ? <Badge status="error" text="Inactivo"/> :
            <Badge status="success" text="activo"/>),
    },
    {
        title: 'Opciones',
        key: 'opcions',
        fixed: 'right',
        width: 190,
        render: (_, {id}: DataType) => {
            return <Link to={`/details/${id}`}>
                <FileTextOutlined className="text-custom_blue text-sm"/>
            </Link>},
    },
];
