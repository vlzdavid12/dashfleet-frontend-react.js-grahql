import React, {useState} from 'react';
import {useNavigate } from "react-router-dom";
import {Outlet} from 'react-router-dom';
import logo from '../assets/img/logo.png';
import logo2 from '../assets/img/logo_2.png';
import type { MenuProps } from 'antd';
import '../index.scss';
import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    UserOutlined,
    FormOutlined,
    UnorderedListOutlined
} from '@ant-design/icons';
import {
    Avatar,
    Badge,
    Layout,
    Menu,
    theme,
} from 'antd';

const {Header, Sider, Content} = Layout;


const AppLayout: React.FC = () => {
    const [collapsed, setCollapsed] = useState(false);
    const navigate = useNavigate();
    const {
        token: {colorBgContainer},
    } = theme.useToken();

    const onClick: MenuProps['onClick'] = (e) => {
        return navigate(e.key);
    };

    return (<Layout>
        <Sider
            style={{
                overflow: 'auto',
                height: '180vh',
                left: 0,
                top: 0,
                bottom: 0,
            }}
            trigger={null} collapsible collapsed={collapsed}>
            {collapsed ? ( <img className="logo" src={logo2} alt="logo" />) : (<img className="logo" src={logo} alt="logo" />)}

            <Menu
                theme="dark"
                mode="inline"
                defaultSelectedKeys={['']}
                onClick={onClick}
                items={[
                    {
                        key: '',
                        icon: <UnorderedListOutlined />,
                        label: 'Pedidos',
                    },
                    {
                        key: 'form',
                        icon: <FormOutlined />,
                        label: 'Crear Pedido',
                    },
                ]}
            />
        </Sider>
        <Layout className="site-layout">
            <Header style={{padding: 0, background: colorBgContainer}} className="flex justify-between">
                {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                    className: 'trigger',
                    onClick: () => setCollapsed(!collapsed),
                })}
                <div></div>
                <div className="pr-4 flex gap-4">
                  <div className="avatar-item">
                        <Badge count={1}>
                         <Avatar icon={<UserOutlined/>}/>
                        </Badge>
                  </div>
                    <div className="block relative" style={{top: '-10px'}}>
                    <p className="h-4">David Valenzuela ...</p>
                    <span className="text-small">vlzdavid12@outlook.com</span>
                </div>
                </div>
            </Header>
            <Content
                style={{
                    margin: '24px 16px',
                    padding: 24,
                    background: colorBgContainer,
                }}
            >
                <Outlet/>
            </Content>
        </Layout>
    </Layout>);
}

export default AppLayout;
