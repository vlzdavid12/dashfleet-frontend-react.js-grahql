import React from "react";
import {TablePaginationConfig} from "antd/es/table";
import {FilterValue} from "antd/es/table/interface";

export interface DataType {
    key: React.Key;
    id?: string,
    codeRequest: string,
    fullName: string,
    dateRequest: string,
    status: boolean
}

export interface TableParams {
    pagination?: TablePaginationConfig;
    sortField?: string;
    sortOrder?: string;
    filters?: Record<string, FilterValue>;
}
