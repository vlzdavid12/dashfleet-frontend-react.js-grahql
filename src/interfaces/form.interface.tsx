export interface Values {
    fullName: string,
    codeRequest: string,
    typeDocument: string,
    numberDocument: string,
    dateRequest: string,
    address: string,
    status: boolean
}
