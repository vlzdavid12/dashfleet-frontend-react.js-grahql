import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import reportWebVitals from './reportWebVitals';
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import Layout from "./shared/Layout";
import InvoicePage from "./pages/invoices.page";
import DetailsPage from "./pages/details.page";
import FormPage from "./pages/form.page";
import ErrorPage from "./pages/error.page";
import {ApolloProvider} from "@apollo/client";
import client from "./config/apollo";


const router = createBrowserRouter([{
    path: '/',
    element: <Layout/>,
    children: [
        {
            index: true,
            element: <InvoicePage/>,
            errorElement: <ErrorPage/>
        }, {
            path: '/form',
            element: <FormPage/>,
            errorElement: <ErrorPage/>
        }, {
            path: '/details/:id',
            element: <DetailsPage/>,
            errorElement: <ErrorPage/>
        },
    ],
},
    {
        path: '*',
        element: <ErrorPage/>
    }])

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);
root.render(
    <React.StrictMode>
        <ApolloProvider client={client}>
        <RouterProvider router={router}/>
        </ApolloProvider>
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
