import {Field, Form, Formik} from "formik";
import {SearchValues} from "../interfaces/search.interface";
import {SearchOutlined, CloseCircleOutlined} from "@ant-design/icons";
import {gql, useLazyQuery} from "@apollo/client";
import {Modal, Table} from "antd";
import React, {useState} from "react";
import {columns} from "../helpers/columns_table";
import {DataType} from "../interfaces/invoices.interface";

const QUERY_SEARCH = gql(`
    query getSearchInvoices($offset: Int, $limit: Int, $search:[String!]){
        invoices(
          offset: $offset
          limit: $limit
          search: $search
        ){
        id
        fullName
        dateRequest
        codeRequest
        numberDocument
        status
        }
    }

`);

const SearchComponent = () => {
    const [open, setOpen] = useState(false);

    const [getSearch, {loading, error, data}] = useLazyQuery(QUERY_SEARCH, { fetchPolicy: 'network-only'});

    const initial: SearchValues = {
        requestCode: '',
        typeDocument: '',
        numberDocument: ''
    }

    if(loading) return <p>Loading ...</p>

    const tableData: DataType[] = [];

    if (data) {
        data.invoices.map((items: DataType, index: number) => {
            console.log(items)
            tableData.push({
                key: index,
                id: items.id,
                codeRequest: items.codeRequest,
                fullName: items.fullName,
                dateRequest: items.dateRequest,
                status: items.status
            });
            return null;
        })
    }

    return <>
        <Modal
            title={null}
            style={{ top: 40 }}
            open={open}
            footer={null}
            onOk={() => setOpen(false)}
            onCancel={() => setOpen(false)}
            width={1000}
        >
            {error ? (<div className="text-center">
                    <CloseCircleOutlined className="text-lg" />
                    <p>{error.message}</p>
            </div>):
                (<div>
                    <Table columns={columns} dataSource={tableData} pagination={false}/>
                </div>)}

        </Modal>
        <Formik initialValues={initial} onSubmit={(
            values: SearchValues
        ) => {
            const {requestCode, typeDocument, numberDocument} = values;

            getSearch({
                variables: {
                    offset: 1,
                    limit: 10,
                    search: [requestCode, typeDocument, numberDocument]
                }
            });
            setOpen(true)
        }}>

            <Form className="flex gap-3">
                <Field name="requestCode" placeholder="Codigo Pedido" className="form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
    "/>

                <Field as="select" className="
                form-select appearance-none
      block
      w-full
      px-3
      py-1.5
      text-base
      font-normal
      text-gray-700
      bg-white bg-clip-padding bg-no-repeat
      border border-solid border-gray-300
      rounded
      transition
      ease-in-out
      m-0
      focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                       placeholder="Seleccione tipo de pedido"
                       name="typeDocument">
                    <option value="">Ingresa el tipo de pedido</option>
                    <option value="a">Por objetivo</option>
                    <option value="b">Por forma</option>
                    <option value="c">Por el proveedor</option>
                    <option value="d">Por destino</option>
                </Field>

                <Field name="numberDocument" placeholder="Número Documento" className="form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
    "/>
                <button
                    className="flex items-center justify-center gap-2 inline-block px-6 py-2.5 bg-custom text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-custom hover:text-white hover:shadow-lg focus:bg-custom focus:shadow-lg focus:outline-none focus:ring-0 active:bg-custom active:shadow-lg transition duration-150 ease-in-out"
                    type="submit">
                    <SearchOutlined/>
                </button>
            </Form>

        </Formik>
    </>
}

export default SearchComponent;
