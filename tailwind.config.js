/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors')

module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      emerald: colors.emerald,
      indigo: colors.indigo,
      yellow: colors.yellow,
      red: colors.red,
      blue: colors.blue,
      custom: '#fec32b',
      custom_blue: '#001529'
    },
    fontSize: {
      small: '0.789rem',
      xs: '0.8rem',
      md: '1rem',
      sm: '2rem',
      lg: '5rem',
      xl: '7rem',
      '2xl': '12rem',
      '3xl': '18rem',
      '4xl': '20rem',
      '5xl': '24rem',
    }
  },
  plugins: [],
}
